/**
 * Created by deniskashuba on 27.02.16.
 */

var SigmaSDKMenu = (function () {

	var answersToValid = [200, 304];
	var URIOfMenuJson = './menu.json';
	var URIOfStyles = './sigmaMenuSdkStyles.css';
	var ElementIdToBuildIn = 'sigmaMenu';
	var wrapperEl = null;

	var menuObject = {};

	/*

		Handlers section

	 */
	/*

		@return - xmlhttp object

	 */
	function getXmlHttp() {

		var xmlhttp;

		try {
			xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (E) {
				xmlhttp = false;
			}
		}

		if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
			xmlhttp = new XMLHttpRequest();
		}

		return xmlhttp;

	}

	/*

	 	string textToParse - text to parse in to object
	 	@return - return parsed object

	 */
	function parseJson(textToParse) {

		var a = null;

		try {

			a = JSON.parse(textToParse);

		} catch (e) {

			console.log(e);

		}

		return a;

	}

	/*

		tring elId - element id
		@return - element object from DOM

	 */
	function getElById(elId) {

		var el = null;

		if (elId !== undefined && elId !== '')
			el = document.getElementById(elId);
		else
			console.log('Please specify id for element');

		return el;

	}

	/*

		string type - type of builded object
		string className - class for new object
		string data - data to insert in to new object
		@return created element

	 */
	function createEl(type, className, data, config) {

		var retEl = null;

		switch (type) {
			case 'ul':
				retEl = document.createElement('ul');
				retEl.id = data.title + '_'+ data.group_id;
				if (className !== undefined && className)
					retEl.className = className;
				break;
			case 'li':
				retEl = document.createElement('li');
				if (className !== undefined && className)
					retEl.className = className;
				if (config && config.innerhtml !== false)
					retEl.innerHTML = data.title;
				if (data && data.color !== undefined)
					retEl.style.backgroundColor = data.color;
				break;
			case 'a':
				retEl = document.createElement('a');
				if (className !== undefined && className)
					retEl.className = className;
				if (data.description !== undefined && data.description !== '')
					retEl.title = data.description;
				if (config && config.innerhtml !== false)
					retEl.innerHTML = data.title;
				retEl.href = data.url;
				break;
			default:
				console.log('el with type ' + type + ' unhandled');
		}

		return retEl;

	}

	/*

		DOM el mainEl - main element
		DOM el appenedEl - element which will be aded

	 */
	function appedToEl(mainEl, appenedEl) {

		mainEl.appendChild(appenedEl);

	}
	/*

	 Handlers section

	 */


	/*

		get menu JSON from remote server

	 */
	function getMenuJson(force, cb) {

		if (menuObject !== undefined &&
			menuObject &&
			typeof menuObject === 'object' &&
			Object.keys(menuObject).length > 0 &&
			force !== true) {

			console.log('Object is fine, nothing to update');
			if (cb)
				cb(false);

		}

		if (URIOfMenuJson !== undefined &&
			URIOfMenuJson !== '' &&
			URIOfMenuJson) {

			var xmlhttp = getXmlHttp()
			xmlhttp.open('GET', URIOfMenuJson, true);

			xmlhttp.onreadystatechange = function () {
				if (xmlhttp.readyState == 4) {
					if (answersToValid.indexOf(xmlhttp.status) >= 0) {

						menuObject = parseJson(xmlhttp.responseText);

						if (menuObject === null)
							console.log('Something wrong with menu object');
						else {

							if (cb)
								cb(menuObject);

						}

					} else {

						console.log('We got invalid HTTP code ' + xmlhttp.status);
						if (cb)
							cb(false);

					}
				}
			};
			xmlhttp.send(null);

		} else {

			console.log('JSON URI is invalid!');
			if (cb)
				cb(false);

		}

	}

	/*

		get and insert styles

	 */
	function getStyles() {

		if (URIOfStyles !== undefined && URIOfStyles && URIOfStyles !== '') {

			var link = document.createElement('link');
			link.setAttribute('rel', 'stylesheet');
			link.setAttribute('type', 'text/css');
			link.setAttribute('href', URIOfStyles);
			document.getElementsByTagName('head')[0].appendChild(link);
			return true;

		} else {

			return false;

		}

	}

	/*

		remove whole menu

	 */
	function removeMenu(cb) {

		wrapperEl = getElById(ElementIdToBuildIn);

		if (wrapperEl !== null) {

			console.log('Removing menu');

			while (wrapperEl.firstChild) {
				wrapperEl.removeChild(wrapperEl.firstChild);
			}

			if (cb)
				return cb();

		}

		console.log('Cant remove menu because wrapper element is null');

		if (cb)
			return cb();

	}

	/*

		flow to build menu

	 */
	function buildMenu() {

		getMenuJson(true, function (MenuArr) {

			if (MenuArr !== false) {

				getStyles();

				removeMenu(function () {

					rendererMenu();

				});

			}

		});

	}

	/*

	 	flow to renderer menu

	 */
	function rendererMenu() {

		getMenuJson(false, function (MenuArr) {

			if (MenuArr !== false && MenuArr.groups !== undefined && MenuArr.groups.length) {

				var mainEl = getElById(ElementIdToBuildIn);

				var whichGroupToBuild = mainEl.getAttribute('group_id');

				for (var index in MenuArr.groups) {

					var item = MenuArr.groups[index];

					if (item &&
						whichGroupToBuild &&
						item.group_id !== undefined &&
						item.group_id != whichGroupToBuild)
							continue;

					if (item && item.title !== undefined) {

						var ulEl = createEl('ul', 'wrapper', item);

						//var liEl = createEl('li', 'headerEl', item, {
						//	innerhtml : true
						//});
						//appedToEl(ulEl, liEl);

						if (item && item.menu_items !== undefined && item.menu_items.length) {

							for (var subIndex in item.menu_items) {

								var subMenuData = item.menu_items[subIndex];

								var liEl = createEl('li', 'tile headerEl', subMenuData);
								var aEl = createEl('a', 'subLink', subMenuData, {
									innerhtml : true
								});
								appedToEl(liEl, aEl);
								appedToEl(ulEl, liEl);

							}

						}

						appedToEl(mainEl, ulEl);

					}else {

						console.log('corrupted menu item!');

					}

				}

			}

		});

	}

	buildMenu();

	return {
		reBuildMenu: function () {

			buildMenu();

		},
		removeMenu: function () {

			removeMenu(function () {

				console.log('removed');

			});

		},
		getMenuObj: function (force, cb) {

			getMenuJson(force, function (menuObj) {

				console.log(menuObj);

				if (cb)
					cb(menuObj);

			});

		}
	}

}());

